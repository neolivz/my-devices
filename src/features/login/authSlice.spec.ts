import authReducer, { AuthState, logout } from "./authSlice";

describe("auth reducer", () => {
  const initialState: AuthState = {
    status: "succeeded",
    token: "token",
    user: { email: "email@email.com" },
  };
  it("should handle initial state", () => {
    expect(authReducer(undefined, { type: "unknown" })).toEqual({
      user: null,
      token: null,
      status: "uninitiated",
    });
  });

  it("should handle logout", () => {
    const actual = authReducer(initialState, logout());
    expect(actual.status).toEqual("uninitiated");
    expect(actual.token).toEqual(null);
    expect(actual.user).toEqual(null);
  });
});
