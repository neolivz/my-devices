import { createSlice } from "@reduxjs/toolkit";
import { api as deviceApi, Device } from "./devicesApi";
import type { RootState } from "../../app/store";
import { logout } from "../login/authSlice";

type DeviceState = {
  devices: Device[];
  status: "failed" | "succeeded" | "processing" | "uninitiated";
};

const slice = createSlice({
  name: "devicesApi",
  initialState: { devices: [], status: "uninitiated" } as DeviceState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addCase(logout, (state) => {
      state.devices = [];
      state.status = "uninitiated";
    });

    builder.addMatcher(deviceApi.endpoints.devices.matchPending, (state) => {
      // if we are polling we should not show empty list
      if(state.status === 'uninitiated'){
        state.devices = [];
        state.status = "processing";
      }
    });

    builder.addMatcher(
      deviceApi.endpoints.devices.matchFulfilled,
      (state, { payload }) => {
        state.devices = payload.devices;
        state.status = "succeeded";
      }
    );

    builder.addMatcher(deviceApi.endpoints.devices.matchRejected, (state) => {
      // We should not show failure message for polling response
      if(state.status === 'processing') {
        state.devices = [];
        state.status = "failed";
      }
    });
  },
});

export default slice.reducer;

export const selectDevices = (state: RootState) => state.devices.devices;
export const selectDevicesStatus = (state: RootState) => state.devices.status;
