import { createSlice } from "@reduxjs/toolkit";
import { api as authApi, User } from "./authApi";
import type { RootState } from "../../app/store";

export type AuthState = {
  user: User | null;
  token: string | null;
  status: "failed" | "succeeded" | "processing" | "uninitiated";
};

const slice = createSlice({
  name: "authApi",
  initialState: { user: null, token: null, status: "uninitiated" } as AuthState,
  reducers: {
    logout: (state) => {
      state.status = 'uninitiated';
      state.token = null;
      state.user = null;
    },
  },
  extraReducers: (builder) => {
    builder.addMatcher(
      authApi.endpoints.login.matchPending,
      (state) => {
        state.status = "processing";
      }
    );

    builder.addMatcher(
      authApi.endpoints.login.matchFulfilled,
      (state, { payload }) => {
        state.user = payload.user;
        state.token = payload.token;
      }
    );

    builder.addMatcher(authApi.endpoints.login.matchRejected, (state) => {
      state.status = "failed";
    });
  },
});



export default slice.reducer;
export const { logout } = slice.actions;

export const selectCurrentUser = (state: RootState) => state.auth.user;
export const selectIsUserLoggedIn = (state: RootState) => state.auth.user !== null;
