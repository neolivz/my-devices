import { createSlice } from "@reduxjs/toolkit";
import { api as notifyApi } from "./notifyApi";
import type { RootState } from "../../app/store";

type NotifyState = {
  message: string;
  status: "failed" | "succeeded" | "processing" | "uninitiated";
};

const slice = createSlice({
  name: "notifyApi",
  initialState: { status: "uninitiated" } as NotifyState,
  reducers: {},
  extraReducers: (builder) => {
    builder.addMatcher(notifyApi.endpoints.notify.matchPending, (state) => {
      state.status = "processing";
    });

    builder.addMatcher(
      notifyApi.endpoints.notify.matchFulfilled,
      (state, {payload}) => {
        state.message = payload.message;
        state.status = "succeeded";
      }
    );

    builder.addMatcher(notifyApi.endpoints.notify.matchRejected, (state) => {
      state.status = "failed";
    });
  },
});

export default slice.reducer;

export const selectNotifyMessage = (state: RootState) => state.notify.message;
export const selectNotifyStatus = (state: RootState) => state.notify.status;
