import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { RootState } from "../../app/store";

export interface Device {
  id: number;
  name: string;
}

export interface DeviceAPIResponse {
    devices: Device[];
}

export const api = createApi({
  reducerPath: 'devicesData',
  baseQuery: fetchBaseQuery({
    baseUrl: "http://35.201.2.209:8000/",
    prepareHeaders: (headers, { getState }) => {
      // By default, if we have a token in the store, let's use that for authenticated requests
      const token = (getState() as RootState).auth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    devices: builder.query<DeviceAPIResponse, void>({
      query: () => ({
        url: "devices",
        method: "GET",
      }),
    }),
  }),
});

export const { useDevicesQuery, useLazyDevicesQuery } = api;
