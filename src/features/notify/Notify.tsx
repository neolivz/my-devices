import React, { useState } from "react";
import { Container, Input, TextArea, Button} from '../../components'

import { useAppSelector } from "../../app/hooks";

import { useNotifyMutation } from "./notifyApi";
import { selectNotifyMessage } from "./notifySlice";

export function Notify() {
  const repoUrl = "https://bitbucket.org/neolivz/my-devices/";

  const notificationReply = useAppSelector(selectNotifyMessage);

  const [message, setMessage] = useState("");
  const [email, setEmail] = useState("");
  const [name, setName] = useState("");

  const [notifyMutation, { isLoading: isProcessing }] = useNotifyMutation();

  const notify = () => {
    notifyMutation({
      email,
      message,
      repoUrl,
      name: "Jishnu",
    });
  };


  return (
    <Container >
      <div>
        <Container>
          Feedback
        </Container>

          <TextArea
            id="message"
            label="Message"
            name="message"
            onChange={(e) => setMessage(e.target.value)}
            value={message}
            autoFocus
          />

          <Input
            name="name"
            label="Name"
            type="name"
            id="name"
            onChange={(e) => setName(e.target.value)}
            value={name}
            autoComplete="name"
          />
          <Input
            name="email"
            label="Email"
            type="email"
            id="email"
            onChange={(e) => setEmail(e.target.value)}
            value={email}
            autoComplete="email"
          />
          <Container>
            {notificationReply}
          </Container>

          <Button
            type="submit"
            disabled={isProcessing}
            onClick={notify}
          >
            Notify
          </Button>
      </div>
    </Container>
  );
}
