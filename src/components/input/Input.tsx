import React, { InputHTMLAttributes, useRef } from "react";
import uniqid from "uniqid";

import styled from "styled-components";

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  id?: string;
  label: string;
  error?: string;
  forceError?: boolean;
  forceErrorMessage?: string;
}

type PartialInputProps = Partial<InputProps>;

const InputContainer = styled.div<PartialInputProps>`
  position: relative;
  padding: 15px 0 0;
  margin-top: 10px;
`;

const InputField = styled.input<PartialInputProps>`
  font-family: inherit;
  width: 100%;
  border: 0;
  border-bottom: ${({ forceError }) =>
    !forceError ? "1px solid #d2d2d2" : "1px solid red"};
  outline: 0;
  font-size: 16px;
  color: #212121;
  padding: 7px 0;
  background: transparent;
  transition: border-color 0.2s;
  ::placeholder {
    color: transparent;
  }
  &:invalid {
    border: 1px solid red;
  }
  &:focus {
    padding-bottom: 6px;
    border-bottom: ${({ forceError }) =>
      !forceError ? "2px solid #009788" : "2px solid red"};
  }
`;

const LabelContainer = styled.label`
  position: absolute;
  top: 0;
  display: block;
  transition: 0.2s;
  font-size: 12px;
  color: #9b9b9b;
  ${InputField}:focus ~ & {
    color: #009788;
  }
  ${InputField}:placeholder-shown:not(:focus) ~ & {
    font-size: 16px;
    cursor: text;
    top: 20px;
  }
`;

const ErrorContainer = styled.div`
  font-size: 12px;
  color: transparent;
  ${InputField}:invalid ~ & {
    color: red;
  }
`;

const ForcedErrorContainer = styled.div`
  font-size: 12px;
  color: red;
`;

export const Input: React.FC<InputProps> = ({
  id,
  label,
  error = "Invalid Field",
  forceError,
  forceErrorMessage,
  onChange,
  ...restOfProps
}) => {
  const inputId = useRef(id || `md-input-${uniqid()}`);

  return (
    <InputContainer>
      <InputField
        forceError={forceError}
        id={inputId.current}
        placeholder={label}
        onChange={onChange}
        {...restOfProps}
      />

      <LabelContainer htmlFor={inputId.current}>{label}</LabelContainer>
      {!forceError ? <ErrorContainer>{error}</ErrorContainer> : null}
      {forceError ? (
        <ForcedErrorContainer>{forceErrorMessage}</ForcedErrorContainer>
      ) : null}
    </InputContainer>
  );
};
