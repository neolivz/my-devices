import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { RootState } from "../../app/store";

export interface NotifyAPIRequest {
  name: string;
  email: string;
  repoUrl: string;
  message: string;
}

export interface NotifyAPIResponse {
  message: string;
}

export const api = createApi({
  reducerPath: "notifyApi",
  baseQuery: fetchBaseQuery({
    baseUrl: "http://35.201.2.209:8000/",
    prepareHeaders: (headers, { getState }) => {
      // By default, if we have a token in the store, let's use that for authenticated requests
      const token = (getState() as RootState).auth.token;
      if (token) {
        headers.set("authorization", `Bearer ${token}`);
      }
      return headers;
    },
  }),
  endpoints: (builder) => ({
    notify: builder.mutation<NotifyAPIResponse, NotifyAPIRequest>({
      query: (data) => ({
        url: "notify",
        method: "POST",
        body: data,
        responseHandler: (response) => {
          return new Promise(async (resolve, reject) => {
            const message = await response.text();
            console.log({message})
            resolve({
              message,
            });
          });
        },
      }),
    }),
  }),
});

export const { useNotifyMutation } = api;
