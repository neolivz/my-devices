import React, { useState, useEffect } from "react";
import { Button, Container, Input } from "../../components/";

import { useLoginMutation } from "./authApi";

export function Login() {

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [apiError, setApiError] = useState(false);
  const [loginMutation, { isLoading: isLoggingIn, error }] = useLoginMutation();

  useEffect(() => {
    setApiError(!!error)
  }, [error]);

  useEffect(() => {
    setApiError(false);
  },[email, password])


  const login = () => {
    loginMutation({ email, password });
  };
  return (
    <Container>
      <div>
        <Container>Sign in</Container>
        <form>
          <Input
            forceError={apiError}
            required
            label="E-mail"
            id="email"
            name="email"
            type="email"
            autoComplete="email"
            error="Invalid Email Address"
            onChange={(e) => setEmail(e.target.value)}
            value={email}
            autoFocus
          />
          <Input
            forceErrorMessage="Incorrect Email Or Password"
            forceError={apiError}
            required
            label="Password"
            name="password"
            type="password"
            id="password"
            onChange={(e) => setPassword(e.target.value)}
            value={password}
            autoComplete="current-password"
          />

          <Button
            type="submit"
            color="primary"
            disabled={isLoggingIn}
            onClick={login}
          >
            Sign In
          </Button>
        </form>
      </div>
    </Container>
  );
}
