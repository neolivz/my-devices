import React from "react";
import styled from "styled-components";

import { Button } from "../../components";
import { useAppDispatch, useAppSelector } from "../../app/hooks";

import { selectDevices, selectDevicesStatus } from "./deviceSlice";
import { useDevicesQuery } from "./devicesApi";
import { logout } from "../login/authSlice";

const Card = styled.div``;
const AppBar = styled.div`
  width: 100vw;
  height: 100px;
  background-color: #880e4f;
`;
const Table = styled.table`
  padding: 2em;
`;
const TableHeadRow = styled.thead`
  border: 1px solid #880e4f;
`;
const TableHead = styled.th`
  background: #ad1457;
  color: #fff;
  padding: 1em;
  padding-left: 2em;
  padding-right: 2em;
`;
const TableBody = styled.tbody`
  border: 1px solid #880e4f;
`;
const TableRow = styled.tr`
  &:nth-child(even) {
    background-color: #f8bbd0;
  }
`;
const TableCell = styled.td`
  text-align: center;
  padding: 0.5em;
`;

export function Devices() {
  useDevicesQuery(undefined, {
    pollingInterval: 5000,
    refetchOnMountOrArgChange: true,
  });
  const devices = useAppSelector(selectDevices);
  const status = useAppSelector(selectDevicesStatus);

  const dispatch = useAppDispatch();

  if (status === "uninitiated") {
    return <div> Devices Not Loaded, uninitiated</div>;
  }

  if (status === "processing") {
    return <div>Loading</div>;
  }

  if (status === "failed") {
    return <div> Devices Loading Failed</div>;
  }

  if (!devices || devices.length === 0) {
    return <div>No Device</div>;
  }

  return (
    <Card>
      <AppBar>
        <Button onClick={() => dispatch(logout())}> Logout</Button>
      </AppBar>
      <Table>
        <TableHeadRow>
          <TableRow>
            <TableHead>Id</TableHead>
            <TableHead>Name</TableHead>
          </TableRow>
        </TableHeadRow>
        <TableBody>
          {devices.map((device) => (
            <TableRow key={device.id}>
              <TableCell>{device.id}</TableCell>
              <TableCell align="left">{device.name}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </Card>
  );
}
