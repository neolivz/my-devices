import {
  configureStore,
  combineReducers,
  ThunkAction,
  Action,
} from "@reduxjs/toolkit";
import {
  persistStore,
  persistReducer,
  FLUSH,
  REHYDRATE,
  PAUSE,
  PERSIST,
  PURGE,
  REGISTER,
} from "redux-persist";
import storage from "redux-persist/lib/storage";

import authReducer from "../features/login/authSlice";
import deviceReducer from "../features/devices/deviceSlice";
import notifyReducer from "../features/notify/notifySlice";
import { api as authApi } from "../features/login/authApi";
import { api as devicesApi } from "../features/devices/devicesApi";
import { api as notifyApi } from "../features/notify/notifyApi";

const persistConfig = {
  key: "root",
  version: 1,
  storage,
};

const rootReducer = combineReducers({
  [authApi.reducerPath]: authApi.reducer,
  [devicesApi.reducerPath]: devicesApi.reducer,
  [notifyApi.reducerPath]: notifyApi.reducer,
  auth: authReducer,
  devices: deviceReducer,
  notify: notifyReducer,
});

const persistedReducer = persistReducer(persistConfig, rootReducer);

export const store = configureStore({
  reducer: persistedReducer,
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware({
      serializableCheck: {
        ignoredActions: [FLUSH, REHYDRATE, PAUSE, PERSIST, PURGE, REGISTER],
      },
    })
      .concat(notifyApi.middleware)
      .concat(devicesApi.middleware)
      .concat(authApi.middleware),
});

export const persistor = persistStore(store);

export type AppDispatch = typeof store.dispatch;
export type RootState = ReturnType<typeof store.getState>;
export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  unknown,
  Action<string>
>;
