import styled from 'styled-components';

export const Button = styled.button`
  background: #3D5AFE;
  color: #FFF;
  font-size: 1em;
  margin: 1em;
  padding: 1em 2em;
  border: 2px solid #E8EAF6;
  border-radius: 3px;
`;

