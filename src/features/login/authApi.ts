import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

export interface User {
  email: string;
}

export interface UserResponse {
  user: User;
  token: string;
}

export interface LoginRequest {
  email: string;
  password: string;
}

export const api = createApi({
  reducerPath: 'authApi',
  baseQuery: fetchBaseQuery({
    baseUrl: "http://35.201.2.209:8000/",
  }),
  endpoints: (builder) => ({
    login: builder.mutation<UserResponse, LoginRequest>({
      query: (credentials) => ({
        url: "login",
        method: "POST",
        body: credentials,
        responseHandler: (response) => {
          return new Promise(async (resolve, reject) => {
            const token = await response.text();
            resolve({
              user: {
                email: credentials.email,
              },
              token,
            })
          })
        },
      }),
    }),
  }),
});

export const { useLoginMutation } = api;
