import React from "react";

import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from "react-router-dom";

import { Devices } from "./features/devices/Devices";
import { Notify } from "./features/notify/Notify";
import { Login } from "./features/login/Login";
import { selectIsUserLoggedIn } from "./features/login/authSlice";
import { useAppSelector } from "./app/hooks";


function App() {
  const isUserLoggedIn = useAppSelector(selectIsUserLoggedIn);
  return (
    <Router>
      <Switch>
        {!isUserLoggedIn ? (
          <Route path="/login">
            <Login />
          </Route>
        ) : null}
        {!isUserLoggedIn ? <Redirect from="/" to="/login" /> : null}
        {isUserLoggedIn ? (
          <Route path="/devices">
            <Devices />
            <Notify />
          </Route>
        ) : null}

        {isUserLoggedIn ? <Redirect from="/" to="/devices" /> : null}
      </Switch>
    </Router>
  );
}

export default App;
